import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

import { GetListResponse } from './api.types';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getList<T>(path: string): Observable<GetListResponse<T>> {
    return this.http.get<T[]>(environment.apiUrl + path).pipe(
      map(response => ({ response, error: null })),
      catchError(val => of({ response: [], error: val })),
    );
  }
}
