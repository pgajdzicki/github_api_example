import { HttpErrorResponse } from '@angular/common/http';

export interface GetListResponse<T> {
  response: T[];
  error: HttpErrorResponse;
}
