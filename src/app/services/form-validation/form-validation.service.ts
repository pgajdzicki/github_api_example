export class FormValidationService {
  static getFormValidationMessage(validator: string) {
    const messages = {
      required: 'This field is required',
      whitespace: 'This field cannot contain whitespaces',
    };

    return messages[validator] || validator;
  }
}
