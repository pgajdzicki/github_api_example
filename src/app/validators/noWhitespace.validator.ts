import { FormControl, ValidationErrors } from '@angular/forms';

export const noWhitespaceValidator = (control: FormControl): ValidationErrors => {
  const hasWhitespace = String(control.value || '').indexOf(' ') >= 0;

  return hasWhitespace ? { whitespace: true } : null;
};
