import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UserRepository } from '@modules/repositories/repositories.types';
import { ActivatedRoute } from '@angular/router';
import { GetListResponse } from '@services/api';

@Component({
  selector: 'app-repositories-list',
  templateUrl: './repositories-list.component.html',
  styleUrls: ['./repositories-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RepositoriesListComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}

  items: GetListResponse<UserRepository>;
  username: string;

  ngOnInit() {
    const {
      snapshot: {
        data: { items },
        params: { username },
      },
    } = this.route;

    this.username = username;
    this.items = items;
  }
}
