import { Component } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { noWhitespaceValidator } from '@validators';

@Component({
  selector: 'app-repository-find',
  templateUrl: './repository-find.component.html',
  styleUrls: ['./repository-find.component.scss'],
})
export class RepositoryFindComponent {
  constructor(private router: Router, private formBuilder: FormBuilder) {}

  repositoryForm = this.formBuilder.group({
    username: new FormControl('', {
      validators: [Validators.required, noWhitespaceValidator],
    }),
  });

  onSubmit() {
    this.repositoryForm.markAllAsTouched();

    if (this.repositoryForm.valid) {
      this.router.navigate(['/repositories/list', this.repositoryForm.value.username]);
    }
  }
}
