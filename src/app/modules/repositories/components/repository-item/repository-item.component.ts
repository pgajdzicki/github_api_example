import { Component, Input } from '@angular/core';
import { UserRepository } from '@modules/repositories/repositories.types';

@Component({
  selector: 'app-repository-item',
  templateUrl: './repository-item.component.html',
  styleUrls: ['./repository-item.component.scss'],
})
export class RepositoryItemComponent {
  @Input() repository: UserRepository;
}
