import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RepositoriesRoutingModule } from './repositories-routing.module';

import { RepositoriesService } from './repositories.service';
import { RepositoriesListResolverService } from './resolvers/repositories-list.resolver.service';

import { RepositoriesListComponent } from './components/repositories-list/repositories-list.component';
import { RepositoryItemComponent } from './components/repository-item/repository-item.component';
import { RepositoryFindComponent } from './components/repository-find/repository-find.component';
import { FormsErrorComponent } from 'src/app/components/forms-error/forms-error.component';

@NgModule({
  declarations: [RepositoriesListComponent, RepositoryItemComponent, RepositoryFindComponent, FormsErrorComponent],
  imports: [CommonModule, RepositoriesRoutingModule, ReactiveFormsModule],
  providers: [RepositoriesService, RepositoriesListResolverService],
})
export class RepositoriesModule {}
