import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RepositoriesListComponent } from './components/repositories-list/repositories-list.component';
import { RepositoryFindComponent } from './components/repository-find/repository-find.component';
import { RepositoriesListResolverService } from './resolvers/repositories-list.resolver.service';

const routes: Routes = [
  { path: '', redirectTo: 'find', pathMatch: 'full' },
  { path: 'find', component: RepositoryFindComponent },
  {
    path: 'list/:username',
    component: RepositoriesListComponent,
    resolve: { items: RepositoriesListResolverService },
  },
  {
    path: '**',
    redirectTo: 'find',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RepositoriesRoutingModule {}
