import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GetListResponse } from '@services/api';

import { UserRepository } from '../repositories.types';
import { RepositoriesService } from '../repositories.service';

@Injectable({
  providedIn: 'root',
})
export class RepositoriesListResolverService implements Resolve<GetListResponse<UserRepository>> {
  constructor(private repositoriesService: RepositoriesService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.repositoriesService.getUserRepositoriesWithoutForks(route.params['username']);
  }
}
