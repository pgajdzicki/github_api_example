import { Injectable } from '@angular/core';
import { ApiService } from '@services/api';
import { UserRepository } from './repositories.types';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RepositoriesService {
  constructor(private apiService: ApiService) {}

  getUserRepositories(username: string) {
    return this.apiService.getList<UserRepository>(`/users/${username}/repos`);
  }

  getUserRepositoriesWithoutForks(username: string) {
    return this.getUserRepositories(username).pipe(
      map(item => ({ ...item, response: item.response.filter(repo => !repo.fork) })),
    );
  }
}
