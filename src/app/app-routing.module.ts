import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'repositories', pathMatch: 'full' },
  {
    path: 'repositories',
    loadChildren: () => import('@modules/repositories/repositories.module').then(m => m.RepositoriesModule),
  },
  {
    path: '**',
    redirectTo: 'repositories',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
