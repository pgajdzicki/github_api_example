import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FormValidationService } from '@services/form-validation';

@Component({
  selector: 'app-forms-error',
  templateUrl: './forms-error.component.html',
  styleUrls: ['./forms-error.component.scss'],
})
export class FormsErrorComponent {
  @Input() control: FormGroup | FormControl;

  get error() {
    for (let key in this.control.errors) {
      if (this.control.errors.hasOwnProperty(key) && this.control.touched) {
        return FormValidationService.getFormValidationMessage(key);
      }
    }

    return null;
  }
}
